delete from transactions;

delete from limits;

insert into limits
(id, limit_category, limit_date_time, limit_sum, balance)
values (1, 'PRODUCT', '2025-05-21 16:24:39', 1000, 1000);

insert into transactions(id, account_from, account_to, category, currency, date_time, limit_exceeded, sum, limit_id) values
(1, 1111111111, 2222222222, 'PRODUCT', 'USD', '2024-05-21 16:24:39', true, 1100, 1);

insert into transactions(id, account_from, account_to, category, currency, date_time, limit_exceeded, sum, limit_id) values
(2, 1111111111, 2222222222, 'PRODUCT', 'USD', '2024-05-21 16:24:39', true, 1500, 1);