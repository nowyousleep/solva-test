package ru.solomennikov.currencyApi;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import ru.solomennikov.currencyApi.dto.TransactionDto;
import ru.solomennikov.currencyApi.enums.CurrencyType;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TransactionsControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private ObjectMapper objectMapper;


    @Test
    @Sql(value = {"/scripts/delete-transactions-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void whenOutOfLimitTransactionsDoNotExist_thenThrowNotFoundException() throws Exception {
        this.mockMvc.perform(get("/transactions/out_of_limit"))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    @Sql(value = {"/scripts/add-transactions-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void whenGetTransactions_thenReturnListOfDto() throws Exception {
        this.mockMvc.perform(get("/transactions/out_of_limit"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.transactions").exists());
    }

    @Test
    @Sql(value = {"/scripts/add-limit-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void whenAddNewTransactions_thenReturnDto() throws Exception {
        TransactionDto dto = new TransactionDto(1111111111L, 2222222222L, CurrencyType.USD,
                new BigDecimal("100"), ExpenseCategory.PRODUCT);
        this.mockMvc.perform(post("/transactions")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.currency").value("USD"))
                .andExpect(jsonPath("$.sum").value(100));
    }
}