package ru.solomennikov.currencyApi.service;

import ru.solomennikov.currencyApi.dto.TransactionDto;

import java.util.List;

public interface TransactionService {
    TransactionDto saveTransaction(TransactionDto dto);

    List<TransactionDto> getOutOfLimitTransactions();
}
