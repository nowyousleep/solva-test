package ru.solomennikov.currencyApi.service.helpers;

import org.springframework.stereotype.Component;
import ru.solomennikov.currencyApi.persist.entity.LimitEntity;
import java.time.LocalDate;

@Component
public class LimitsMonthsComparator {
    public int compareLimitsMonths(LimitEntity entity) {
        return entity.getLimitDate().getMonth().compareTo(LocalDate.now().getMonth());
    }
}
