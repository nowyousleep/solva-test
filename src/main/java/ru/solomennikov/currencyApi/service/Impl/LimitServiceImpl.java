package ru.solomennikov.currencyApi.service.Impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import ru.solomennikov.currencyApi.dto.LimitDto;
import ru.solomennikov.currencyApi.mapper.LimitMapper;
import ru.solomennikov.currencyApi.persist.entity.LimitEntity;
import ru.solomennikov.currencyApi.persist.repository.LimitRepository;
import ru.solomennikov.currencyApi.service.LimitService;
import ru.solomennikov.currencyApi.service.helpers.LimitsMonthsComparator;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class LimitServiceImpl implements LimitService {

    private final LimitRepository limitRepository;

    private final LimitsMonthsComparator comparator;
    private final LimitMapper limitMapper = Mappers.getMapper(LimitMapper.class);

    /**
     * Перед сохранением нового месячного лимита ищет предыдущий лимит.
     * Если он находится и его месяц совпадает с текущим, то пересчитываем остаток нового лимита.
     * В ином случае выставляем остаток нового лимита равным сумме этого лимита.
     *
     * @return LimitDto
     */
    @Override
    public LimitDto saveNewLimit(LimitDto dto) {
        LimitEntity newLimit = limitMapper.mapToEntity(dto);
        Optional<LimitEntity> optionalLimit = limitRepository
                .findFirstByLimitCategoryOrderByLimitDateDesc(dto.getLimitCategory());
        if (optionalLimit.isPresent() && comparator.compareLimitsMonths(optionalLimit.get()) == 0) {
            LimitEntity oldLimit = optionalLimit.get();
            BigDecimal newLimitBalanceAmount = newLimit.getLimitSum()
                    .subtract(oldLimit.getLimitSum())
                    .add(oldLimit.getBalance());
            newLimit.setBalance(newLimitBalanceAmount);
        } else {
            newLimit.setBalance(newLimit.getLimitSum());
        }
        newLimit.setLimitDate(LocalDateTime.now());
        LimitEntity savedEntity = limitRepository.save(newLimit);
        log.info("limit saved: {}", savedEntity);
        return limitMapper.mapToDto(savedEntity);
    }
}
