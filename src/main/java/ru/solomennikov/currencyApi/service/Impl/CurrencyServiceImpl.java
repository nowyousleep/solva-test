package ru.solomennikov.currencyApi.service.Impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.solomennikov.currencyApi.enums.CurrencyType;
import ru.solomennikov.currencyApi.persist.entity.CurrencyEntity;
import ru.solomennikov.currencyApi.persist.entity.CurrencyEntityPK;
import ru.solomennikov.currencyApi.persist.repository.CurrencyRepository;
import ru.solomennikov.currencyApi.service.CurrencyService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@Setter
@Getter
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository repository;

    private final ObjectMapper mapper;

    @Value("${external-source.apikey}")
    private String apiKey;

    @Value("${external-source.url}")
    private String url;

    @Bean
    private RestTemplate template() {
        return new RestTemplate();
    }

    @Scheduled(fixedRate = 86_400_000)
    public void currencyResponse() {
        try {
            String response = template().getForObject(url + apiKey, String.class);
            JsonNode obj = mapper.readTree(response);

            BigDecimal kzt = new BigDecimal(obj.get("quotes").get("USDKZT").toString());
            BigDecimal kztRate = new BigDecimal("1").divide(kzt, 6, RoundingMode.HALF_UP);

            BigDecimal rub = new BigDecimal(obj.get("quotes").get("USDRUB").toString());
            BigDecimal rubRate = new BigDecimal("1").divide(rub, 6, RoundingMode.HALF_UP);

            long timestamp = obj.get("timestamp").longValue();
            LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.systemDefault());

            CurrencyEntityPK rubEntityPK = new CurrencyEntityPK(CurrencyType.RUB, dateTime);
            CurrencyEntityPK kztEntityPK = new CurrencyEntityPK(CurrencyType.KZT, dateTime);

            CurrencyEntity rubEntity = new CurrencyEntity(rubEntityPK, rubRate);
            CurrencyEntity kztEntity = new CurrencyEntity(kztEntityPK, kztRate);

            List<CurrencyEntity> rates = new ArrayList<>();
            rates.add(rubEntity);
            rates.add(kztEntity);

            repository.saveAll(rates);
        } catch (NullPointerException | HttpClientErrorException | JsonProcessingException ex) {
            log.error(ex.getMessage());
        }
    }
}
