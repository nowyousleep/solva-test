package ru.solomennikov.currencyApi.service.Impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import ru.solomennikov.currencyApi.dto.TransactionDto;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;
import ru.solomennikov.currencyApi.exception.InternalServerException;
import ru.solomennikov.currencyApi.mapper.TransactionMapper;
import ru.solomennikov.currencyApi.persist.entity.CurrencyEntity;
import ru.solomennikov.currencyApi.persist.entity.LimitEntity;
import ru.solomennikov.currencyApi.persist.entity.TransactionEntity;
import ru.solomennikov.currencyApi.persist.repository.CurrencyRepository;
import ru.solomennikov.currencyApi.persist.repository.LimitRepository;
import ru.solomennikov.currencyApi.persist.repository.TransactionRepository;
import ru.solomennikov.currencyApi.service.TransactionService;
import ru.solomennikov.currencyApi.service.helpers.LimitsMonthsComparator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    private final LimitsMonthsComparator comparator;

    private final TransactionRepository transactionRepository;

    private final CurrencyRepository currencyRepository;

    private final LimitRepository limitRepository;
    private final TransactionMapper transactionMapper = Mappers.getMapper(TransactionMapper.class);

    /**
     * Перед сохранением новой транзакции ищет самый последний месячный лимит.
     * Если лимит находится и его месяц совпадает с текущим, то пересчитывает остаток лимита.
     * Иначе создаётся новый лимит в 1000 USD.
     *
     * @return TransactionDto
     */
    @Override
    public TransactionDto saveTransaction(TransactionDto dto) {
        ExpenseCategory category = dto.getCategory();
        LimitEntity limit = new LimitEntity();
        CurrencyEntity currency = currencyRepository.findByCode(dto.getCurrency()).orElseThrow(() ->
                InternalServerException.withMessage("Can't create transaction. There is no valid currency rate"));
        Optional<LimitEntity> optionalLimit = limitRepository.findFirstByLimitCategoryOrderByLimitDateDesc(category);
        TransactionEntity transactionEntity = transactionMapper.mapToEntity(dto);
        if (optionalLimit.isPresent() && comparator.compareLimitsMonths(optionalLimit.get()) == 0) {
            limit = optionalLimit.get();
            BigDecimal newBalanceAmount = limit.getBalance()
                    .subtract(dto.getSum().multiply(currency.getRate()));
            limit.setBalance(newBalanceAmount);
        } else {
            limit.setLimitDate(LocalDateTime.now());
            limit.setLimitCategory(dto.getCategory());
            limit.setLimitSum(new BigDecimal("1000"));
            BigDecimal newLimitBalance = limit.getLimitSum()
                    .subtract(dto.getSum().multiply(currency.getRate()));
            limit.setBalance(newLimitBalance);
            limitRepository.save(limit);
        }
        transactionEntity.setLimitExceeded(limit.getBalance().compareTo(new BigDecimal("0")) < 0);
        transactionEntity.setLimit(limit);
        transactionEntity.setDateTime(LocalDateTime.now());
        TransactionEntity savedTransaction = transactionRepository.save(transactionEntity);
        log.info("transaction saved: {}", savedTransaction);
        return transactionMapper.mapToDto(savedTransaction);
    }

    @Override
    public List<TransactionDto> getOutOfLimitTransactions() {
        List<TransactionEntity> transactionEntities = transactionRepository.findByLimitExceededTrue();
        List<TransactionDto> transactionDtoList = new ArrayList<>(transactionEntities.size());
        for (TransactionEntity transaction : transactionEntities) {
            CurrencyEntity currency = currencyRepository.findByDateAndCode(transaction.getDateTime(), transaction.getCurrency());
            TransactionDto dto = transactionMapper.mapToDto(transaction);
            dto.setUsdSum(transaction.getSum().multiply(currency.getRate()).setScale(2, RoundingMode.HALF_UP));
            transactionDtoList.add(dto);
        }
        return transactionDtoList;
    }
}
