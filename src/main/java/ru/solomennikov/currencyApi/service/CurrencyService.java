package ru.solomennikov.currencyApi.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface CurrencyService {
    void currencyResponse() throws JsonProcessingException;
}
