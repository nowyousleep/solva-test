package ru.solomennikov.currencyApi.service;

import ru.solomennikov.currencyApi.dto.LimitDto;

public interface LimitService {
    LimitDto saveNewLimit(LimitDto limit);
}
