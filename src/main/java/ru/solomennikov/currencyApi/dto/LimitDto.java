package ru.solomennikov.currencyApi.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Monthly limit entity")
public class LimitDto {

    @JsonProperty("limit_sum")
    @NotNull(message = "can't be null")
    @DecimalMin(value = "0", message = "can't be less or equals 0", inclusive = false)
    @Schema(description = "Limit sum")
    private BigDecimal limitSum;

    @JsonProperty("limit_date")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    @Schema(description = "Limit date", accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime limitDate;

    @JsonProperty("limit_category")
    @NotNull(message = "can't be null")
    @Schema(description = "Expense category")
    private ExpenseCategory limitCategory;

    public LimitDto(BigDecimal sum, ExpenseCategory category) {
        this.limitSum = sum;
        this.limitCategory = category;
    }

}
