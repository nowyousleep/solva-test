package ru.solomennikov.currencyApi.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.solomennikov.currencyApi.enums.CurrencyType;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Transaction entity")
public class TransactionDto {

    @JsonProperty("account_from")
    @Min(value = 1_000_000_000L, message = "wrong sender account number")
    @Max(value = 9_999_999_999L, message = "wrong sender account number")
    @Schema(description = "Sender account number")
    @NotNull(message = "can't be null")
    private long accountFrom;

    @Min(value = 1_000_000_000L, message = "wrong receiver account number")
    @Max(value = 9_999_999_999L, message = "wrong receiver account number")
    @JsonProperty("account_to")
    @Schema(description = "Receiver account number")
    @NotNull(message = "can't be null")
    private long accountTo;

    @NotNull(message = "can't be null")
    @Schema(description = "Transaction currency")
    @JsonProperty("currency")
    private CurrencyType currency;

    @NotNull(message = "can't be null")
    @Schema(description = "Transaction amount")
    @DecimalMin(value = "0", message = "cant be less or equals 0", inclusive = false)
    @JsonProperty("sum")
    private BigDecimal sum;

    @Schema(description = "Transaction USD amount")
    @JsonProperty(value = "usd_sum", access = JsonProperty.Access.READ_ONLY)
    private BigDecimal usdSum;

    @NotNull(message = "can't be null")
    @Schema(description = "Expense category")
    @JsonProperty("category")
    private ExpenseCategory category;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    @JsonProperty(value = "date_time", access = JsonProperty.Access.READ_ONLY)
    @Schema(description = "Date of transaction", accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime dateTime;

    @Schema(description = "Limit associated with this transaction", accessMode = Schema.AccessMode.READ_ONLY)
    @JsonProperty(value = "limit", access = JsonProperty.Access.READ_ONLY)
    private LimitDto limit;

    public TransactionDto(long accountFrom, long accountTo, CurrencyType currency,
                          BigDecimal sum, ExpenseCategory category) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.currency = currency;
        this.sum = sum;
        this.category = category;
    }
}
