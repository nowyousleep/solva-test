package ru.solomennikov.currencyApi.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.solomennikov.currencyApi.dto.TransactionDto;
import ru.solomennikov.currencyApi.enums.CurrencyType;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Response with created transaction")
public class TransactionResponse {

    @JsonProperty("account_from")
    @Schema(description = "Sender account number")
    private long accountFrom;

    @JsonProperty("account_to")
    @Schema(description = "Receiver account number")
    private long accountTo;

    @Schema(description = "Transaction currency")
    private CurrencyType currency;

    @Schema(description = "Transaction sum")
    private BigDecimal sum;

    @Schema(description = "Expense category")
    private ExpenseCategory category;

    @JsonProperty("date_time")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    @Schema(description = "Date of transaction")
    private LocalDateTime dateTime;

    public TransactionResponse(TransactionDto transactionDto) {
        this.accountFrom = transactionDto.getAccountFrom();
        this.accountTo = transactionDto.getAccountTo();
        this.currency = transactionDto.getCurrency();
        this.dateTime = transactionDto.getDateTime();
        this.sum = transactionDto.getSum();
        this.category = transactionDto.getCategory();
    }
}
