package ru.solomennikov.currencyApi.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.solomennikov.currencyApi.dto.TransactionDto;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "List of transactions that exceeded the limit")
public class OutOfLimitTransactionsResponse {

    @JsonProperty("transactions")
    private List<TransactionDto> transactions;
}
