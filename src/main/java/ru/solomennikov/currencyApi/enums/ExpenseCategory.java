package ru.solomennikov.currencyApi.enums;

import lombok.Getter;

@Getter
public enum ExpenseCategory {
    PRODUCT, SERVICE
}
