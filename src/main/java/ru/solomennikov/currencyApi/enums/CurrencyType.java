package ru.solomennikov.currencyApi.enums;

import lombok.Getter;

@Getter
public enum CurrencyType {
    KZT, RUB, USD
}
