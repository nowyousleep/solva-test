package ru.solomennikov.currencyApi.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public abstract class ErrorDto {

    @Schema(description = "Information")
    private String info;

    @JsonProperty("time_stamp")
    @Schema(description = "Date and time")
    private LocalDateTime timeStamp;

}
