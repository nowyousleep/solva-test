package ru.solomennikov.currencyApi.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Schema(description = "Validation errors")
@JsonPropertyOrder({"fieldName", "info", "timeStamps"})
public class ValidationErrorDto extends ErrorDto {

    @Schema(description = "Field name")
    @JsonProperty("field_name")
    private String fieldName;


    public ValidationErrorDto(String field, String info, LocalDateTime date) {
        super(info, date);
        this.fieldName = field;
    }
}
