package ru.solomennikov.currencyApi.exception;

public class InternalServerException extends RuntimeException {

    private static final String ERROR = "Server error: %s";

    private InternalServerException(String message) {
        super(message);
    }

    public static InternalServerException withMessage(String message) {
        return new InternalServerException(ERROR.formatted(message));
    }
}
