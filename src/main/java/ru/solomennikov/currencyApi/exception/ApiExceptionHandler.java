package ru.solomennikov.currencyApi.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<ResponseErrorDto> handleNotFoundException(InternalServerException ex) {
        ResponseErrorDto exception = new ResponseErrorDto();
        exception.setInfo(ex.getMessage());
        exception.setTimeStamp(LocalDateTime.now());
        exception.setStatus(503);
        log.error(exception.getInfo());
        return new ResponseEntity<>(exception, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler
    public ResponseEntity<List<ValidationErrorDto>> validationExceptions(MethodArgumentNotValidException ex) {
        final List<ValidationErrorDto> errors = ex.getBindingResult().getFieldErrors().stream()
                .map(error -> new ValidationErrorDto(error.getField(), error.getDefaultMessage(), LocalDateTime.now()))
                .collect(Collectors.toList());
        for (ValidationErrorDto error : errors) {
            log.error("Validation error: " + error.getFieldName() + " " + error.getInfo());
        }
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ResponseErrorDto> handleJsonExceptions(HttpMessageNotReadableException ex) {
        ResponseErrorDto exception = new ResponseErrorDto();
        if (ex.getMessage().contains("PRODUCT")) {
            exception.setInfo("Invalid category. Please select from SERVICE or PRODUCT");
        } else exception.setInfo("Invalid currency value. Please select from RUB, KZT or USD");
        exception.setTimeStamp(LocalDateTime.now());
        exception.setStatus(400);
        log.error(exception.getInfo());
        return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ResponseErrorDto> handleGeneralExceptions(Exception ex) {
        ResponseErrorDto exception = new ResponseErrorDto();
        exception.setInfo(ex.getMessage());
        exception.setTimeStamp(LocalDateTime.now());
        exception.setStatus(500);
        log.error("Unknown error: " + exception.getInfo());
        return new ResponseEntity<>(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
