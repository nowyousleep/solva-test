package ru.solomennikov.currencyApi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.solomennikov.currencyApi.dto.TransactionDto;
import ru.solomennikov.currencyApi.dto.response.OutOfLimitTransactionsResponse;
import ru.solomennikov.currencyApi.dto.response.TransactionResponse;
import ru.solomennikov.currencyApi.exception.ValidationErrorDto;
import ru.solomennikov.currencyApi.service.TransactionService;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/transactions")
@Tag(name = "Transaction management")
public class TransactionsController {

    private final TransactionService transactionService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Saves new transaction", responses = {
            @ApiResponse(responseCode = "201", content =
            @Content(schema = @Schema(implementation = TransactionResponse.class))),
            @ApiResponse(responseCode = "400", content =
            @Content(array = @ArraySchema(schema = @Schema(implementation = ValidationErrorDto.class))))})
    public ResponseEntity<TransactionResponse> addTransaction(@RequestBody @Valid TransactionDto dto) {
        TransactionDto transactionDto = transactionService.saveTransaction(dto);
        TransactionResponse response = new TransactionResponse(transactionDto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(value = "/out_of_limit", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Displays a list of transactions that have exceeded the monthly limit", responses = {
            @ApiResponse(responseCode = "200", content =
            @Content(schema = @Schema(implementation = OutOfLimitTransactionsResponse.class))),
            @ApiResponse(responseCode = "204", content =
            @Content(schema = @Schema(example = "")))})
    public ResponseEntity<OutOfLimitTransactionsResponse> getOutOfLimitTransactions() {
        List<TransactionDto> transactionDtoList = transactionService.getOutOfLimitTransactions();
        if (transactionDtoList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        OutOfLimitTransactionsResponse response = new OutOfLimitTransactionsResponse(transactionDtoList);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
