package ru.solomennikov.currencyApi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.solomennikov.currencyApi.dto.LimitDto;
import ru.solomennikov.currencyApi.exception.ValidationErrorDto;
import ru.solomennikov.currencyApi.service.LimitService;

@RestController
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Monthly limits management")
public class LimitsController {

    private final LimitService limitService;

    @PostMapping(value = "/limits", consumes = MediaType.APPLICATION_JSON_VALUE, produces =
            MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Allows you to set a new monthly limit", responses = {
            @ApiResponse(responseCode = "201", content =
            @Content(schema = @Schema(implementation = LimitDto.class))),
            @ApiResponse(responseCode = "400", content =
            @Content(array = @ArraySchema(schema = @Schema(implementation = ValidationErrorDto.class))))
    })
    public ResponseEntity<LimitDto> addNewLimit(@RequestBody @Valid LimitDto dto) {
        LimitDto savedLimit = limitService.saveNewLimit(dto);
        return new ResponseEntity<>(savedLimit, HttpStatus.CREATED);
    }
}
