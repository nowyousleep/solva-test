package ru.solomennikov.currencyApi.persist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;
import ru.solomennikov.currencyApi.persist.entity.LimitEntity;

import java.util.Optional;

@Repository
public interface LimitRepository extends JpaRepository<LimitEntity, Integer> {
    Optional<LimitEntity> findFirstByLimitCategoryOrderByLimitDateDesc(ExpenseCategory category);
}
