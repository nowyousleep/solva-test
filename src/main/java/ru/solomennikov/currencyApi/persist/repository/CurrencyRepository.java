package ru.solomennikov.currencyApi.persist.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;
import ru.solomennikov.currencyApi.enums.CurrencyType;
import ru.solomennikov.currencyApi.persist.entity.CurrencyEntity;
import ru.solomennikov.currencyApi.persist.entity.CurrencyEntityPK;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface CurrencyRepository extends CassandraRepository<CurrencyEntity, CurrencyEntityPK> {
    @Query("SELECT * FROM currency_rates WHERE date_time < ?0 and currency_code = ?1 ORDER BY date_time DESC limit 1")
    CurrencyEntity findByDateAndCode(LocalDateTime dateTime, CurrencyType currencyCode);

    @Query("SELECT * FROM currency_rates WHERE currency_code = ?0 ORDER BY date_time DESC limit 1")
    Optional<CurrencyEntity> findByCode(CurrencyType currencyCode);
}
