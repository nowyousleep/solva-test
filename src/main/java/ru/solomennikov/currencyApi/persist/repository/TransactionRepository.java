package ru.solomennikov.currencyApi.persist.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.solomennikov.currencyApi.persist.entity.TransactionEntity;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {
    @Query("select t from TransactionEntity t join fetch t.limit where t.limitExceeded = true")
    List<TransactionEntity> findByLimitExceededTrue();
}
