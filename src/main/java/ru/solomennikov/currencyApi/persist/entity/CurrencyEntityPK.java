package ru.solomennikov.currencyApi.persist.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import ru.solomennikov.currencyApi.enums.CurrencyType;

import java.time.LocalDateTime;

@Data
@PrimaryKeyClass
@AllArgsConstructor
public class CurrencyEntityPK {

    @Id
    @Column("currency_code")
    @PrimaryKeyColumn(name = "currency_code", type = PrimaryKeyType.PARTITIONED)
    @JsonProperty("currency_code")
    private CurrencyType currencyCode;

    @Id
    @JsonProperty("date_time")
    @Column("date_time")
    @PrimaryKeyColumn(name = "date_time", type = PrimaryKeyType.CLUSTERED)
    private LocalDateTime dateTime;
}
