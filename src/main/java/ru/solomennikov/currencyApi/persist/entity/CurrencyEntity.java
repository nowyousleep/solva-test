package ru.solomennikov.currencyApi.persist.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table("currency_rates")
public class CurrencyEntity {

    @PrimaryKey
    private CurrencyEntityPK currencyEntityPK;

    @Column("rate")
    private BigDecimal rate;
}
