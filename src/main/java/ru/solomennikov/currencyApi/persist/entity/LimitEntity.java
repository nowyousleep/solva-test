package ru.solomennikov.currencyApi.persist.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "limits")
public class LimitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "limit_sum")
    private BigDecimal limitSum;

    @Column(name = "limit_date_time")
    private LocalDateTime limitDate;

    @Column(name = "limit_category")
    @Enumerated(EnumType.STRING)
    private ExpenseCategory limitCategory;

    @Column(name = "balance")
    private BigDecimal balance;

    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "limit_id")
    private List<TransactionEntity> transactions;

    @Override
    public String toString() {
        return "LimitEntity {" +
                "id=" + id +
                ", limitSum=" + limitSum +
                ", limitDate=" + limitDate +
                ", limitCategory=" + limitCategory +
                ", balance=" + balance +
                '}';
    }
}
