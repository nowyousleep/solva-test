package ru.solomennikov.currencyApi.persist.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.solomennikov.currencyApi.enums.CurrencyType;
import ru.solomennikov.currencyApi.enums.ExpenseCategory;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
@Getter
@Setter
@NoArgsConstructor
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "account_from")
    private long accountFrom;

    @Column(name = "account_to")
    private long accountTo;

    @Column(name = "currency")
    @Enumerated(EnumType.STRING)
    private CurrencyType currency;

    @Column(name = "sum")
    private BigDecimal sum;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private ExpenseCategory category;

    @Column(name = "date_time")
    private LocalDateTime dateTime;

    @Column(name = "limit_exceeded")
    private Boolean limitExceeded;

    @ManyToOne
    private LimitEntity limit;

    @Override
    public String toString() {
        return "TransactionEntity {" +
                "id=" + id +
                ", accountFrom=" + accountFrom +
                ", accountTo=" + accountTo +
                ", currency=" + currency +
                ", sum=" + sum +
                ", category=" + category +
                ", dateTime=" + dateTime +
                ", limitExceeded=" + limitExceeded +
                '}';
    }
}
