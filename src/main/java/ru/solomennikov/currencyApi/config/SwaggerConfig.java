package ru.solomennikov.currencyApi.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                title = "Test task",
                description = "Service for receiving transactions", version = "1.0.0",
                contact = @Contact(
                        name = "Solomennikov Evgeny",
                        email = "zheka21-91@mail.ru"
                )
        )
)
public class SwaggerConfig {
}

