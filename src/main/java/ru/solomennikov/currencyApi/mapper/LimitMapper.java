package ru.solomennikov.currencyApi.mapper;

import org.mapstruct.Mapper;
import ru.solomennikov.currencyApi.dto.LimitDto;
import ru.solomennikov.currencyApi.persist.entity.LimitEntity;

@Mapper
public abstract class LimitMapper implements AbstractMapper<LimitDto, LimitEntity> {
}
