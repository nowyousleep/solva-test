package ru.solomennikov.currencyApi.mapper;

import org.mapstruct.Mapper;
import ru.solomennikov.currencyApi.dto.TransactionDto;
import ru.solomennikov.currencyApi.persist.entity.TransactionEntity;

@Mapper
public abstract class TransactionMapper implements AbstractMapper<TransactionDto, TransactionEntity> {

}
