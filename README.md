## About the project
### Microservice prototype for the banking system.

**The microservice performs the following functions:**
1. Receives real-time information about each debit transaction in tenge (KZT),  
rubles (RUB) and US dollars (USD) and saves it in its own database.
2. Stores a monthly spending limit in US dollars (USD)  
separately for two categories of spending: goods and services. If not set, accept limit as 1000 USD.
3. Requests exchange rate data for KZT/USD, RUB/USD currency pairs on a daily basis  
and stores them in its own database.
4. Flags indicators that have exceeded the monthly transaction limit.
5. Allows the client to set a new monthly limit.
6. At the request of the client, returns a list of transactions that have exceeded the limit,  
indicating the limit that was exceeded.

## How to run project

Docker is required to run this project. You should go to the folder with the project and run:

    docker-compose up

Then you have to execute the initial script for Cassandra database:

    docker-compose exec cassandra bash -c "cqlsh -f /scripts/init-script.cql"
