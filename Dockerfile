FROM openjdk:19
WORKDIR /src
EXPOSE ${SERVER_PORT}
COPY target/currencyApi-1.0.0.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]

